Pod::Spec.new do |s|
  s.name                    = 'LTLootsie'
  s.version                 = '5.1.6'
  s.license                 = 'MIT'
  s.summary                 = 'The World’s Most Powerful Loyalty Engine'
  s.homepage                = 'http://www.lootsie.com'
  s.social_media_url        = 'https://twitter.com/lootsie'
  s.authors                 = { 'Fabio Teles' => 'pixel4@gmail.com' }
  s.source                  = { :git => 'https://bitbucket.org/LootsieIOS/ios-sdk', :tag => s.version, :submodules => true }
  s.default_subspec         = 'AdMob'
  s.requires_arc            = true
  s.ios.deployment_target   = '7.0'

  s.subspec 'Core' do |ss|
    ss.source_files         = 'LTLootsie/**/*.{h,m}'
    ss.public_header_files  = 'LTLootsie/**/*.h'
    ss.private_header_files = 'LTLootsie/Vendor/**/*.h'
    ss.frameworks           = 'Security', 'SystemConfiguration', 'MobileCoreServices'
  end

  s.subspec 'UI' do |ss|
    ss.dependency 'LTLootsie/Core'

    ss.source_files         = 'LTLootsieUI/**/*.{h,m}'
    ss.public_header_files  = 'LTLootsieUI/**/*.h'
    ss.private_header_files = 'LTLootsieUI/Vendor/**/*.h'
    ss.frameworks           = 'CoreGraphics'
    ss.resources            = ['LTLootsieUI/Resources/*.{storyboard,xib}', 'LTLootsieUI/Resources/Images/*.png']
  end

  s.subspec 'AchievementCore' do |ss|
    ss.dependency 'LTLootsie/Core'
    ss.source_files         = 'LTModule/LTAchievement/Core/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTAchievement/Core/**/*.h'
    ss.xcconfig = {'GCC_PREPROCESSOR_DEFINITIONS' => 'ENABLE_ACHIEVEMENT=1'}
  end

  s.subspec 'AchievementUI' do |ss|
    ss.dependency 'LTLootsie/AchievementCore'
    ss.dependency 'LTLootsie/UI'
    ss.source_files         = 'LTModule/LTAchievement/UI/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTAchievement/UI/**/*.h'
  end

  s.subspec 'MarketplaceCore' do |ss|
    ss.dependency 'LTLootsie/Core'
    ss.source_files         = 'LTModule/LTMarketplace/Core/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTMarketplace/Core/**/*.h'
    ss.xcconfig = {'GCC_PREPROCESSOR_DEFINITIONS' => 'ENABLE_MARKETPLACE=1'}
  end

  s.subspec 'MarketplaceUI' do |ss|
    ss.dependency 'LTLootsie/MarketplaceCore'
    ss.dependency 'LTLootsie/UI'
    ss.source_files         = 'LTModule/LTMarketplace/UI/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTMarketplace/UI/**/*.h'
  end

  s.subspec 'InterstitialCore' do |ss|
    ss.dependency 'LTLootsie/MarketplaceCore'
    ss.dependency 'LTLootsie/AchievementCore'
    ss.source_files         = 'LTModule/LTInterstitial/Core/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTInterstitial/Core/**/*.h'
    ss.xcconfig = {'GCC_PREPROCESSOR_DEFINITIONS' => 'ENABLE_INTERSTITIAL=1'}
  end

  s.subspec 'InterstitialUI' do |ss|
    ss.dependency 'LTLootsie/InterstitialCore'
    ss.dependency 'LTLootsie/MarketplaceUI'
    ss.dependency 'LTLootsie/AchievementUI'
    ss.source_files         = 'LTModule/LTInterstitial/UI/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTInterstitial/UI/**/*.h'
  end

  s.subspec 'AccountCore' do |ss|
    ss.dependency 'LTLootsie/Core'
    ss.source_files         = 'LTModule/LTAccount/Core/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTAccount/Core/**/*.h'
    ss.xcconfig = {'GCC_PREPROCESSOR_DEFINITIONS' => 'ENABLE_ACCOUNT=1'}
  end


  s.subspec 'AccountUI' do |ss|
    ss.dependency 'LTLootsie/AccountCore'
    ss.dependency 'LTLootsie/UI'
    ss.source_files         = 'LTModule/LTAccount/UI/**/*.{h,m}'
    ss.public_header_files  = 'LTModule/LTAccount/UI/**/*.h'
  end


  s.subspec 'AdMob' do |ss|
    ss.dependency 'LTLootsie/InterstitialUI'
    ss.dependency 'LTLootsie/AccountUI'
    ss.preserve_paths = 'ThirdParty/GoogleMobileAds.framework'
    ss.vendored_frameworks = 'GoogleMobileAds.framework'
    ss.vendored_frameworks = 'ThirdParty/GoogleMobileAds.framework'
    ss.xcconfig = { 'GCC_PREPROCESSOR_DEFINITIONS' => 'USE_ADMOB=1'}
  end

  s.subspec 'Aerserv' do |ss|
    ss.dependency 'LTLootsie/InterstitialUI'
    ss.preserve_paths = 'ThirdParty/AerServSDK.framework'
    ss.vendored_frameworks = 'AerServSDK.framework'
    ss.vendored_frameworks = 'ThirdParty/AerServSDK.framework'
    ss.xcconfig = {'GCC_PREPROCESSOR_DEFINITIONS' => 'USE_AERSERV=1', 'OTHER_LDFLAGS' => '-ObjC'}
    ss.libraries = 'xml2.2'
  end

end
