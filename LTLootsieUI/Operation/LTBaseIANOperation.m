//  LTBaseIANOperation.m
//  Created by Fabio Teles on 3/25/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTBaseIANOperation.h"
#import "LTMutuallyExclusiveCondition.h"

#import "LTManager.h"

#import "LTIANBaseIANView.h"

NSTimeInterval const LTDefaulIANDuration        = 5.0;
NSInteger const LTIANMissingContextErrorCode    = -201;

@interface LTBaseIANOperation ()

@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIColor *backgroundColor;
@property (nonatomic, strong) NSString *currencyName;
@property (nonatomic, assign) BOOL animated;
@property (nonatomic, strong) void (^action)(void);
@property (nonatomic, strong) UIViewController *(^presentationContextBlock)(void);
@property (nonatomic, strong) NSLayoutConstraint *topPinningConstraint;

@end

@implementation LTBaseIANOperation

- (instancetype)initWithCurrencyName:(NSString *)currencyName
                           textColor:(UIColor *)textColor
                     backgroundColor:(UIColor *)backgroundColor
                              action:(void (^)(void))action
            presentationContextBlock:(UIViewController* (^)(void))presentationContextBlock {
    if (self = [super init]) {
        _currencyName = currencyName;
        _textColor = textColor;
        _backgroundColor = backgroundColor;
        _action = action;
        _presentationContextBlock = presentationContextBlock;
        _animated = YES;
        
        [self addCondition:[LTMutuallyExclusiveCondition mutualExclusiveForClass:[self class]]];
    }
    return self;
}

- (void)execute {
    // check context first
    UIViewController *context = self.presentationContextBlock();
    if (!context || !context.view) {
        // no context to display IAN
        NSDictionary *userInfo = @{
                                   NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Context missing to present IAN", @"LTLootsie", nil),
                                   LTOperationKey: self};
        NSError *error = [NSError errorWithDomain:LTErrorDomain code:LTIANMissingContextErrorCode userInfo:userInfo];
        [self finishWithError:error];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        LTIANBaseIANView *ianView = [self createIANView];
        
        if (self.backgroundColor) ianView.backgroundColor = self.backgroundColor;
        
        [self showIAN:ianView inContext:context animated:self.animated];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((self.duration > 0.0 ? self.duration : LTDefaulIANDuration) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self hideIAN:ianView animated:self.animated];
        });
    });
}

- (void)showIAN:(LTIANBaseIANView *)ian inContext:(UIViewController *)context animated:(BOOL)animated {
    ian.translatesAutoresizingMaskIntoConstraints = NO;
    
    CGFloat overlayLength = [context.topLayoutGuide length];
    ian.translucenceHeight = overlayLength;
    
    NSDictionary *views = NSDictionaryOfVariableBindings(ian);
    [context.view addSubview:ian];
    [context.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[ian]|" options:0 metrics:nil views:views]];
    self.topPinningConstraint = [NSLayoutConstraint constraintWithItem:ian attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:context.view attribute:NSLayoutAttributeTop multiplier:1.0f constant:(animated ? -([self heightIANView] + overlayLength) : 0.f)];
    [context.view addConstraint:self.topPinningConstraint];
    [context.view addConstraint:[NSLayoutConstraint constraintWithItem:ian attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:[self heightIANView] + overlayLength]];
    [context.view layoutIfNeeded];
    
    if (animated) {
        [UIView animateWithDuration:0.4 delay:0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.topPinningConstraint.constant = 0.f;
            [context.view layoutIfNeeded];
        } completion:nil];
    }
}

- (void)closeDidPressed:(id)sender {
    [self hideIAN:self.ianView animated:YES];
}

- (void)handleIANTap:(id)sender {
    if (self.action) self.action();
}


- (void)hideIAN:(LTIANBaseIANView *)ian animated:(BOOL)animated {
    void (^removeAndFinish)(BOOL) = ^(BOOL  finished) {
        [ian removeFromSuperview];
        [self finish:nil];
    };
    
    if (animated) {
        [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.topPinningConstraint.constant = -([self heightIANView] + ian.translucenceHeight);
            [ian.superview layoutIfNeeded];
        } completion:removeAndFinish];
    } else {
        removeAndFinish(YES);
    }
}

#pragma mark - Public

- (LTIANBaseIANView *)createIANView; {
    return nil;
}

- (CGFloat)heightIANView {
    return 181.0f;
}

@end
