//  LTMenuController.m
//  Created by Fabio Teles on 5/19/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTMenuController.h"
#import "LTUIManager.h"
#import "LTPPRevealSideViewController.h"
#import "LTTools.h"

#if ENABLE_ACHIEVEMENT
#import "LTUIManager+Achievement.h"
#endif
#if ENABLE_MARKETPLACE
#import "LTUIManager+Marketplace.h"
#endif
#if ENABLE_ACCOUNT
#import "LTUIManager+Account.h"
#endif

@interface LTMenuController ()

@property (strong, nonatomic) NSMutableArray *borders;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *achievementsButtonHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rewardsButtonHeight;

@end

@implementation LTMenuController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
#if !ENABLE_ACHIEVEMENT
    self.achievementsButton.hidden = YES;
    self.achievementsButtonHeight.constant = 0;
#endif
#if !ENABLE_MARKETPLACE
    self.rewardsButton.hidden = YES;
    self.rewardsButtonHeight.constant = 0;
#endif
#if !ENABLE_ACCOUNT
    self.accountButton.hidden = YES;
#endif
    [self.view updateConstraintsIfNeeded];
	[self addBorders];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)addBorders {
	UIColor *borderColor = [UIColor lightGrayColor];

	[self addTopBorderWithHeight:1.f andColor:borderColor toView:self.rewardsButton];
	[self addBottomBorderWithHeight:1.f andColor:borderColor toView:self.rewardsButton];
	[self addBottomBorderWithHeight:1.f andColor:borderColor toView:self.achievementsButton];
	[self addBottomBorderWithHeight:1.f andColor:borderColor toView:self.termsButton];
	[self addBottomBorderWithHeight:1.f andColor:borderColor toView:self.aboutButton];
	[self addBottomBorderWithHeight:1.f andColor:borderColor toView:self.accountButton];
}

-(void)addTopBorderWithHeight: (CGFloat)height andColor:(UIColor*)color toView:(UIView *)view {
	[self addOneSidedBorderWithFrame:CGRectMake(0, 0, view.frame.size.width, height) andColor:color toView:view];
}

-(void)addBottomBorderWithHeight: (CGFloat)height andColor:(UIColor*)color toView:(UIView *)view {
	[self addOneSidedBorderWithFrame:CGRectMake(0, view.frame.size.height-height, view.frame.size.width, height) andColor:color toView:view];
}

-(void)addOneSidedBorderWithFrame:(CGRect)frame andColor:(UIColor*)color toView:(UIView *)view {
	CALayer *border = [CALayer layer];
	border.frame = frame;
	[border setBackgroundColor:color.CGColor];
	[view.layer addSublayer:border];

	if (nil == self.borders) {
		self.borders = [NSMutableArray array];
	}
	[self.borders addObject:border];
}

- (void)viewDidLayoutSubviews {
	[super viewDidLayoutSubviews];

	CGRect frame;
	for (CALayer *layer in self.borders) {
		frame = layer.frame;
		if (frame.origin.y != 0) frame.origin.y = layer.superlayer.bounds.size.height - frame.size.height;
		frame.size.width = layer.superlayer.bounds.size.width;
		layer.frame = frame;
	}
}

- (IBAction)menuButtonTapped:(UIButton *)sender {
#if ENABLE_MARKETPLACE
	if ([sender isEqual:self.rewardsButton]) {
		[[LTUIManager sharedManager] showRewardsPage];
	}
#endif
#if ENABLE_ACHIEVEMENT
    if ([sender isEqual:self.achievementsButton]) {
		[[LTUIManager sharedManager] showAchievementsPage];
	}
#endif
    if ([sender isEqual:self.termsButton]) {
		[[LTUIManager sharedManager] showTermsPage];
	}
    if ([sender isEqual:self.aboutButton]) {
		[[LTUIManager sharedManager] showAboutPage];
	}
#if ENABLE_ACCOUNT
    if ([sender isEqual:self.accountButton]) {
		[[LTUIManager sharedManager] showAccountPage];
	}
#endif

	[self.revealSideViewController popViewControllerAnimated:YES];
}

@end
