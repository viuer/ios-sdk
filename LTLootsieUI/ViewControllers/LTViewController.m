//  LTViewController.m
//  Created by Fabio Teles on 5/18/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTViewController.h"
#import "LTUIManager.h"
#import "LTTools.h"
#import "LTPPRevealSideViewController.h"

#if ENABLE_ACCOUNT
#import "LTUserAccount.h"
#import "LTManager+Account.h"
#endif

#import "LTSettings.h"
#import "LTData.h"

@interface LTViewController ()

@property (weak, nonatomic) IBOutlet UIView *headerView;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UIButton *menuButton;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UILabel *welcomeLabel;
@property (weak, nonatomic) IBOutlet UIView *statusBar;

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (strong, nonatomic) UIViewController *waitingFirstPage;
@property (copy, nonatomic) NSString *currentSegueIdentifier;

@end

@implementation LTViewController
@synthesize logoImageView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.closeButton.imageEdgeInsets = UIEdgeInsetsMake(13, 13, 13, 13);
    self.menuButton.imageEdgeInsets = UIEdgeInsetsMake(10, 10, 10, 10);
    
    
	// setup header buttons
	[self.closeButton addTarget:self action:@selector(closeButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	[self.menuButton addTarget:self action:@selector(menuButtonTapped:) forControlEvents:UIControlEventTouchUpInside];

	[self updateData];

	self.statusBar.backgroundColor = [LTTools colorWithHex:0xf75452];
    self.statusBar.clipsToBounds = YES;

	// shadow
	CGRect shadowRect = UIEdgeInsetsInsetRect(self.statusBar.layer.bounds, UIEdgeInsetsMake(10.f, -5.f, 0.f, -5.f));
	self.statusBar.layer.shadowColor = [UIColor blackColor].CGColor;
	self.statusBar.layer.shadowOpacity = .5f;
	self.statusBar.layer.shadowOffset = CGSizeZero;
	self.statusBar.layer.shadowRadius = 2.f;
	self.statusBar.layer.shadowPath = [UIBezierPath bezierPathWithRect:shadowRect].CGPath;
    
    CGRect frame = self.statusBar.bounds;
    frame.size.height = 0.0;
    CGRect innersShadowRect = UIEdgeInsetsInsetRect(self.statusBar.layer.bounds, UIEdgeInsetsMake(0.f, -5.f, 25.f, -5.f));
    UIView* innerShadow = [[UIView alloc] initWithFrame: frame];
    innerShadow.backgroundColor = [UIColor blackColor];
    innerShadow.layer.shadowColor = [UIColor blackColor].CGColor;
    innerShadow.layer.shadowOpacity = .35f;
    innerShadow.layer.shadowOffset = CGSizeZero;
    innerShadow.layer.shadowRadius = 4.f;
    innerShadow.layer.shadowPath = [UIBezierPath bezierPathWithRect:innersShadowRect].CGPath;
    [self.statusBar addSubview: innerShadow];

	if (self.waitingFirstPage) {
		[self setFirstPage:self.waitingFirstPage identifier:nil];
		self.waitingFirstPage = nil;
	}
    
#if ENABLE_ACCOUNT
    self.pointsLabel.hidden = NO;
#else
    self.pointsLabel.hidden = YES;
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userUpdated:) name:LTUserAccountUpdatedNotification object:nil];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewDidAppear:(BOOL)animated {
	[super viewDidAppear:animated];

	// Set the current view controller to the one embedded (in the storyboard).
	self.currentViewController = self.childViewControllers.firstObject;
}

- (void)userUpdated:(NSNotification *)notification {
    [self updateData];
}

- (void)updateData {
	if (!self.isViewLoaded) {
		return;
	}
#if ENABLE_ACCOUNT
    [[LTUIManager sharedManager] userAccountWithSuccess:^(LTUserAccount *userAccount) {
        
        // pointBalanceLabel
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setLocale:[NSLocale currentLocale]];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        NSString *pointsStr = [formatter stringFromNumber:[NSNumber numberWithInteger:[LTUIManager sharedManager].data.user.totalPoints]];
        pointsStr = [[NSString alloc] initWithFormat:@"%@ %@", pointsStr, ([LTUIManager sharedManager].data.settings.currencyAbbreviation ?: @"LP")];
        
        self.pointsLabel.text = pointsStr;
        if ([LTUIManager sharedManager].data.user.isGuest) {
            self.welcomeLabel.text = [@"Guest" uppercaseString];
        } else {
            NSString *name = [LTUIManager sharedManager].data.user.firstName;
            if (name.length == 0) {
                self.welcomeLabel.text = [@"Welcome back!" uppercaseString];
            } else {
                self.welcomeLabel.text = [[[NSString alloc] initWithFormat:@"Welcome, %@", name] uppercaseString];
            }
        }
    } failure:nil];
#endif
}

#pragma mark Segue

- (void)setFirstPage:(UIViewController *)pageController identifier:(NSString *)identifier {
	if (self.isViewLoaded) {
		if (!self.currentViewController) {
			[self addChildViewController:pageController];

			pageController.view.frame = self.containerView.bounds;
			pageController.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
			pageController.view.translatesAutoresizingMaskIntoConstraints = YES;

			[self.containerView addSubview:pageController.view];
			[pageController didMoveToParentViewController:self];
            
            self.currentViewController = pageController;
		}
	} else {
		self.waitingFirstPage = pageController;
	}
    if (identifier) self.currentSegueIdentifier = identifier;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
	self.currentSegueIdentifier = segue.identifier;
	[super prepareForSegue:segue sender:sender];

}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
	if ([self.currentSegueIdentifier isEqual:identifier]) {
		//Dont perform segue, if visible ViewController is already the destination ViewController
		return NO;
	}

	return YES;
}

#pragma mark Actions

- (void)closeButtonTapped:(UIButton *)button {
	[[LTUIManager sharedManager] closeUI];
}

- (void)menuButtonTapped:(UIButton *)button {
	[self.revealSideViewController pushViewController:[[LTUIManager sharedManager] menuController] 
                                          onDirection:LTPPRevealSideDirectionRight
                                           withOffset:50.f
                                             animated:YES];
}

#pragma mark - Orientation stuff

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
	switch ([[LTUIManager sharedManager] renderingMode]) {
		case LTRenderingModeLandscape:
			return UIInterfaceOrientationMaskLandscape;
			break;
		case LTRenderingModePortrait:
			return UIInterfaceOrientationMaskPortrait;
			break;
		case LTRenderingModePortraitAsLandscape:
			return UIInterfaceOrientationMaskLandscape;
			break;
		case LTRenderingModeLandscapeAsPortrait:
			return UIInterfaceOrientationMaskPortrait;
			break;
		default:
			return UIInterfaceOrientationMaskAll;
			break;
	};
}

@end
