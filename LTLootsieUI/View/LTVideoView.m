//  LTVideoView.m
//  Created by Alexander Dovbnya on 2/16/16.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTVideoView.h"

@interface LTVideoView ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIView *progressContainer;

@end

@implementation LTVideoView

+ (Class)layerClass {
    return [AVPlayerLayer class];
}

- (AVPlayer*)player {
    return [(AVPlayerLayer *)[self layer] player];
}

- (void)setPlayer:(AVPlayer *)player {
    [(AVPlayerLayer *)[self layer] setPlayer:player];
    
}

- (void)play {
    [self addTimeObserver];
    [self.player play];
}

- (void)pause {
    [self.player pause];
}

- (void)addTimeObserver {
    __weak LTVideoView *weakSelf = self;
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.frame = self.progressContainer.bounds;
    shapeLayer.path = [UIBezierPath bezierPathWithArcCenter:shapeLayer.position
                                                     radius:CGRectGetMidX(shapeLayer.frame)
                                                 startAngle:-M_PI_2
                                                   endAngle: 3 * M_PI_2
                                                  clockwise:YES].CGPath;
    shapeLayer.strokeStart = 0;
    shapeLayer.lineWidth = 2;
    shapeLayer.strokeColor = [UIColor redColor].CGColor;
    shapeLayer.fillColor = [UIColor colorWithWhite:0 alpha:0.3f].CGColor;
    [self.progressContainer.layer insertSublayer:shapeLayer atIndex:0];
    
    [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 5) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        CGFloat duration = CMTimeGetSeconds(weakSelf.player.currentItem.duration);
        CGFloat timeLeft = duration - CMTimeGetSeconds(time);
        shapeLayer.strokeEnd = timeLeft / duration;
        weakSelf.timeLabel.text = [NSString stringWithFormat:@"%ld", (long)timeLeft];
    }];
}

@end
