//  ViewController.m
//  Created by Fabio Teles on 8/11/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <asl.h>
#import "LTUILootsie.h"
#import "ViewController.h"
#import "LTMWLogging.h"
#if ENABLE_ACHIEVEMENT
#import "LTUIManager+Achievement.h"
#endif

#if ENABLE_MARKETPLACE
#import "LTManager+Marketplace.h"
#import "LTUIManager+Marketplace.h"
#import "LTReward.h"
#endif

#if ENABLE_INTERSTITIAL
#import "LTUIManager+Interstitial.h"
#import "LTVideoInterstitialSequenceOperation.h"
#endif

#if ENABLE_ACCOUNT
#import "LTUIManager+Account.h"
#endif

@interface ViewController () <LTUIManagerDelegate>

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *achievementsButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *marketplaceButtons;

@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *orientationsButtons;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray *accountButtons;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _ltLogUIInput = self.logTextView;
    [LTUIManager sharedManager].delegate = self;
#if !ENABLE_ACHIEVEMENT
    [self disabledMenuButtons:self.achievementsButtons];
#endif
#if !ENABLE_MARKETPLACE
    [self disabledMenuButtons:self.marketplaceButtons];
#endif
#if !ENABLE_ACCOUNT
    [self disabledMenuButtons:self.accountButtons];
#endif
    
    [self disabledMenuButtons: self.orientationsButtons];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)disabledMenuButtons:(NSArray *)buttons {
    for (UIButton *button in buttons) {
        button.enabled = NO;
        button.backgroundColor = [UIColor grayColor];
    }
}

- (IBAction)handleMergePoints:(id)sender {
    [[LTUIManager sharedManager] loginWithEmail:self.emailTextField.text success:nil failure:nil];
}


- (IBAction)handleSendLocation:(id)sender {
    [[LTUIManager sharedManager] sendUserLocationWithSuccess:nil failure:nil];
}

- (IBAction)handleShowAbout:(id)sender {
    [[LTUIManager sharedManager] showAboutPage];
}

- (IBAction)handleShowTermsOfService:(id)sender {
    [[LTUIManager sharedManager] showTermsPage];
}

- (IBAction)handleSetPortrait:(id)sender {
    [LTUIManager sharedManager].renderingMode = LTRenderingModePortrait;
}

- (IBAction)handleSetLandscape:(id)sender {
    [LTUIManager sharedManager].renderingMode = LTRenderingModeLandscape;
}

- (IBAction)handleSetAutoOrient:(id)sender {
    [LTUIManager sharedManager].renderingMode = LTRenderingModeAutomatic;
}


- (IBAction)handleLogout:(id)sender {
    [[LTUIManager sharedManager] logoutWithSuccess:nil failure:nil];
}

- (IBAction)handleLogSwitch:(UISwitch *)sender {
    if (sender.isOn) {
        _ltLogUIInput = self.logTextView;
    } else {
        self.logTextView.text = nil;
        _ltLogUIInput = nil;
    }
}

- (IBAction)handleShowPopup:(id)sender {
    [[LTUIManager sharedManager] showPopupWithTitle:@"Hi" description:@"test" buttonText:@"ok" didCloseAction:nil];
}

#pragma mark - Account

#if ENABLE_ACCOUNT
- (IBAction)handleShowMyAccount:(id)sender {
    [[LTUIManager sharedManager] showAccountPage];
}
#endif

#pragma mark - Marketplace

#if ENABLE_MARKETPLACE

- (IBAction)handleShowRewards:(id)sender {
    [[LTUIManager sharedManager] showRewardsPage];
}

- (IBAction)handleGetRewards:(id)sender {
    [[LTUIManager sharedManager] rewardsWithSuccess:nil failure:nil];
}

- (IBAction)handleRedeemReward:(id)sender {
    [[LTUIManager sharedManager] rewardsWithSuccess:^(NSArray *rewards) {
        LTReward *reward = rewards.lastObject;
        if (reward && [reward isKindOfClass:[LTReward class]]) {
            [[LTUIManager sharedManager] redeemRewardWithId:reward.rewardId
                                        giftOrRegisterEmail:@"lootsietest@lootsie.com"
                                                    success:nil
                                                    failure:nil];
        }
    } failure:nil];
}

#endif

#pragma mark - Achievement

#if ENABLE_ACHIEVEMENT
- (IBAction)handleForceIAN:(id)sender {

    [[LTUIManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
        LTAchievement *achievement = achievements[arc4random_uniform((u_int32_t)achievements.count)];
        if (achievement) {
            [[NSNotificationCenter defaultCenter] postNotificationName:LTAchievementReachedNotification object:[LTUIManager sharedManager] userInfo:@{LTAchievementUserInfoKey: achievement}];
        }
    } failure:nil];
}

- (IBAction)handleShowAchievements:(id)sender {
    [[LTUIManager sharedManager] showAchievementsPage];
}

- (IBAction)handleTestIANVideoAchievement:(id)sender {
    [[LTUIManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
        if (!(achievements.count > 0)) return;
        
        NSArray *repeatables = [achievements filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"repeatable == YES"]];
        LTAchievement *achievement;
        if (repeatables.count > 0) {
            achievement = repeatables[arc4random_uniform((u_int32_t)repeatables.count)];
        } else {
            achievement = achievements[arc4random_uniform((u_int32_t)achievements.count)];
        }
#if ENABLE_INTERSTITIAL
        if (achievement) {
            [[LTUIManager sharedManager] showIANAchievementWithInterstitial:achievement points:achievement.points];
        }
#endif
    } failure:nil];
}

- (IBAction)handleGetAchievements:(id)sender {
    [[LTUIManager sharedManager] achievementsWithSuccess:nil failure:nil];
}

/**
 It'll try to find repeatables achievements and choose a random one. If not repeatable
 achievement is found it'll select a random one between all of them. No-op if no
 achievements are registered.
 */
- (IBAction)handleTestAchievement:(id)sender {
    [[LTUIManager sharedManager] achievementsWithSuccess:^(NSArray *achievements) {
        if (!(achievements.count > 0)) return;
        
        NSArray *repeatables = [achievements filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"repeatable == YES"]];
        LTAchievement *achievement;
        if (repeatables.count > 0) {
            achievement = repeatables[arc4random_uniform((u_int32_t)repeatables.count)];
        } else {
            achievement = achievements[arc4random_uniform((u_int32_t)achievements.count)];
        }
        if (achievement) {
            [[LTUIManager sharedManager] reportAchievementWithId:achievement.achievementId
                                                         success:nil
                                                         failure:^(NSError *error) {
                                                             [[LTUIManager sharedManager] showPopupWithTitle:error.localizedDescription
                                                                                                 description:error.userInfo[NSLocalizedRecoverySuggestionErrorKey]
                                                                                                  buttonText:NSLocalizedStringFromTable(@"Ok", @"LTLootsie", nil)
                                                                                              didCloseAction:nil];
                                                         }];
        }
    } failure:nil];
}

#endif

#pragma mark Video Reward
#if ENABLE_INTERSTITIAL
- (IBAction)handleResetInterstitialCounter:(id)sender {
    [LTVideoInterstitialSequenceOperation resetCounter];
}
#endif

#pragma mark - LTUIManager delegate
- (void)lootsieUIManagerWillShowUI {
    LTLogDebug(@"%@", @"Lootsie UI will show");
}

- (void)lootsieUIManagerDidCloseUI {
    LTLogDebug(@"%@", @"Lootsie UI did close");
}
@end
