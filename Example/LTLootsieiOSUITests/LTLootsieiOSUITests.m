//
//  LTLootsieiOSUITests.m
//  LTLootsieiOSUITests
//
//  Created by Andrew Shapovalov on 4/4/16.
//  Copyright © 2016 Lootsie. All rights reserved.
//

#import <XCTest/XCTest.h>

@interface LTLootsieiOSUITests : XCTestCase

@end

@implementation LTLootsieiOSUITests

- (void)setUp {
    [super setUp];
    
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    // In UI tests it is usually best to stop immediately when a failure occurs.
    self.continueAfterFailure = NO;
    // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
    [[[XCUIApplication alloc] init] launch];
    
    // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // Use recording to get started writing UI tests.
    
    XCUIApplication *app = [[XCUIApplication alloc] init];
    [[[[[[[app.otherElements containingType:XCUIElementTypeTextView identifier:@"outputTextView"] childrenMatchingType:XCUIElementTypeOther] elementBoundByIndex:1] childrenMatchingType:XCUIElementTypeButton] matchingIdentifier:@"Button-showRewardsPage"] elementBoundByIndex:0] tap];
    
    XCUIElementQuery *tablesQuery = app.tables;
    [tablesQuery.staticTexts[@"Watch videos. Earn Points."] tap];
    
    XCUIElementQuery *cellsQuery = [tablesQuery.cells containingType:XCUIElementTypeStaticText identifier:@"50% Off Ultra Light Jacket"];
    [cellsQuery.buttons[@"Details"] tap];
    
    XCUIElement *redeemNowButton = cellsQuery.buttons[@"Redeem Now"];
    [redeemNowButton tap];
    [redeemNowButton tap];
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

@end
