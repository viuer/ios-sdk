//  LTExclusivityController.m
//  Created by Fabio Teles on 7/15/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTExclusivityController.h"
#import "LTOperation.h"

static id _sharedInstance = nil;
@interface LTExclusivityController ()
{
    dispatch_queue_t _serialQueue;
    NSMutableDictionary * _operations;
}
@end

@implementation LTExclusivityController

+ (instancetype)sharedExclusivityController
{
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        _sharedInstance = [self new];
    });
    return _sharedInstance;
}

- (instancetype)init
{
    if (_sharedInstance) {
        return nil;
    }
    if (self = [super init]){
        _serialQueue = dispatch_queue_create("com.lootsie.operations.LTExclusivityController", DISPATCH_QUEUE_SERIAL);
        _operations = [NSMutableDictionary dictionary];
    }
    return self;
}
/// Registers an operation as being mutually exclusive
- (void)addOperation:(LTOperation *)operation categories:(NSArray */*[String]*/)categories
{
    /*
     This needs to be a synchronous operation.
     If this were async, then we might not get around to adding dependencies
     until after the operation had already begun, which would be incorrect.
     */
    dispatch_sync(_serialQueue, ^{
        for (NSString * category in categories) {
            [self noqueue_addOperation:operation category:category];
        }
    });
}

/// Unregisters an operation from being mutually exclusive.
- (void)removeOperation:(LTOperation *)operation categories:(NSArray */*[String]*/)categories
{
    dispatch_async(_serialQueue, ^{
        for (NSString * category in categories) {
            [self noqueue_removeOperation:operation category:category];
        }
    });
}

#pragma mark - Operation Management
- (void)noqueue_addOperation:(LTOperation*)operation category:(NSString *)category
{
    NSMutableArray * operationsWithThisCategory = _operations[category] ?: @[].mutableCopy;
    
    LTOperation * last = operationsWithThisCategory.lastObject;
    if (last) {
        [operation addDependency:last];
    }
    
    [operationsWithThisCategory addObject:operation];
    
    _operations[category] = operationsWithThisCategory;
}

- (void)noqueue_removeOperation:(LTOperation*)operation category:(NSString *)category
{
    NSMutableArray * matchingOperations = _operations[category];
    if (matchingOperations){
        [matchingOperations removeObject:operation];
        _operations[category] = matchingOperations;
    }
}

@end
