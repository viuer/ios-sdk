//  LTURLSessionTaskOperation.m
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTDataTaskOperation.h"
#import "LTNetworkManager.h"
#import "LTMWLogging.h"
#import "LTConstants.h"

static void *URLSessionTaksOperationKVOContext = &URLSessionTaksOperationKVOContext;

static NSString * LTStringFromHTTPBody(NSData *httpBody) {
    if (httpBody) {
        NSString *params = nil;
        params = [[NSString alloc] initWithData:httpBody encoding:NSUTF8StringEncoding];
        params = [params stringByReplacingOccurrencesOfString:@"\",\"" withString:@"\"\n\""];
        params = [params stringByReplacingOccurrencesOfString:@"{" withString:@"{\n"];
        params = [params stringByReplacingOccurrencesOfString:@"}" withString:@"\n}"];
        return params;
    }
    return nil;
}

@interface LTDataTaskOperation ()

@property (nonatomic, strong) LTRequestBuilder requestBuilder;
@property (nonatomic, readwrite, strong) NSURLSessionDataTask *task;
@property (nonatomic, readwrite, strong) NSURLResponse *response;
@property (nonatomic, readwrite, strong) id responseObject;

@end

@implementation LTDataTaskOperation

- (instancetype)initWithRequestBuilder:(LTRequestBuilder)requestBuilder {
    NSParameterAssert(requestBuilder);
    
    if (self = [super init]) {
        _requestBuilder = requestBuilder;
        [self setSafeName:NSStringFromClass([self class])];
    }
    return self;
}

- (void)buildTask {
    // build request
    NSError *requestError;
    self.request = self.requestBuilder(&requestError);
    
    // if it errors out, call completion passing that error along and signal operation complete
    if (requestError) {
        LTLogError(@"Request Builer Failed with %@", requestError.localizedDescription);
        [self finishWithError:requestError];
    }
    
    // build task and return it
    LTLogDebug(@"DataTask Operation with request: %@ %@ %@ %@", self.request.HTTPMethod, self.request.URL, LTStringFromHTTPBody(self.request.HTTPBody), self.request.allHTTPHeaderFields);
    __weak __typeof(self) weakSelf = self;
    self.task = [[LTNetworkManager lootsieManager] dataTaskWithRequest:self.request completionHandler:^(NSURLResponse *response, id responseObject, NSError *error) {
        __strong __typeof(weakSelf) strongSelf = weakSelf;
        
        strongSelf.response = response;
        strongSelf.responseObject = responseObject;
        
        if (!error) error = [strongSelf dataTaskDidFinishSuccessfully];
        
        /** IMPORTANT
         Signal operation complete after returning data from operation for
         who's calling it. Here we cancel operations that failed and finish
         operations that succeed, that way we can differentiate between success
         and failure for data task operations.
         */
        if (error) {
            LTLogError(@"DataTask Operation Failed with: %@", [strongSelf formatDataTaskError:error].localizedDescription);
            [strongSelf cancelWithError:[strongSelf formatDataTaskError:error]];
        } else {
            [strongSelf finishWithError:nil];
        }
    }];
}

- (NSString *)HTTPMethod {
    return self.task.originalRequest.HTTPMethod;
}

- (NSError *)dataTaskDidFinishSuccessfully {
    return nil; // No-op
}

- (NSError *)formatDataTaskError:(NSError *)error {
    if (error && self.responseObject) {
        LTLogError(@"DataTask Operation Failed with Lootsie Error: %@", self.responseObject);
        /**
         SPECIAL error handling back from lootsie server. Creates and
         makes available an error with the information parsed for higher
         level code to use
         */
        NSMutableDictionary *mutableInfo = [error.userInfo mutableCopy];
        NSArray *errors = (NSArray *)self.responseObject[LTErrorErrorsKey];
        if (errors && errors.firstObject) {
            NSDictionary *errorDic = (NSDictionary *)[errors firstObject];
            if (errorDic && errorDic[LTErrorFieldKey] && errorDic[LTErrorMessageKey]) {
                NSString *recoverStr = [NSString stringWithFormat:
                                        NSLocalizedStringFromTable(@"%@ %@",
                                                                   @"LTLootsie",
                                                                   @"{missing|invalid|similar} {field name}"),
                                        errorDic[LTErrorMessageKey],
                                        [errorDic[LTErrorFieldKey] capitalizedStringWithLocale:[NSLocale currentLocale]]];
                
                mutableInfo[NSLocalizedDescriptionKey] = NSLocalizedStringFromTable(@"Uh oh!", @"LTLootsie", nil);
                mutableInfo[NSLocalizedRecoverySuggestionErrorKey] = recoverStr;
            }
        }
        if (errors) mutableInfo[LTNetworkResponseErrorsJSONKey] = errors;
        return [NSError errorWithDomain:LTNetworkErrorDomain
                                   code:error.code
                               userInfo:[NSDictionary dictionaryWithDictionary:mutableInfo]];
    }
    
    if (error) {
        LTLogDebug(@"Returning standard error. Original: %@", error.userInfo);
        return [NSError errorWithDomain:error.domain
                                   code:error.code
                               userInfo:@{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Uh Oh!", @"LTLootsie", nil),
                                          NSLocalizedRecoverySuggestionErrorKey: NSLocalizedStringFromTable(@"Check your Internet connection.", @"LTLootsie", nil),
                                          NSUnderlyingErrorKey: error}];
    }
    
    return  error;
}

- (void)execute {
    [self buildTask];
    [self.task resume];
}

- (void)cancel {
    [self.task cancel];
    [super cancel];
}

+ (NSDictionary *)authHeadersIncludingAppSecret:(BOOL)appSecret apiSession:(BOOL)apiSession userSession:(BOOL)userSession {
    
    if (!appSecret && !apiSession && !userSession) {
        return nil;
    }
    
    id <LTAuthDataSource> authSource = [LTNetworkManager lootsieManager].authDataSource;
    
    /// Logging warnings
    if (appSecret && ![authSource appSecret]) LTLogWarning(@"Required App Secret have not been set yet");
    if (apiSession && ![authSource apiSessionToken]) LTLogWarning(@"Required API Session have not been set yet");
    if (userSession && ![authSource userSessionToken]) LTLogWarning(@"Required User Session have not been set yet");
    
    NSMutableDictionary *headers = [NSMutableDictionary dictionary];
    
    if (appSecret && [authSource appSecret]) headers[LTHeaderAppSecretKey] = [authSource appSecret];
    if (apiSession && [authSource apiSessionToken]) headers[LTHeaderAPISessionTokenKey] = [authSource apiSessionToken];
    if (userSession && [authSource userSessionToken]) headers[LTHeaderUserSessionTokenKey] = [authSource userSessionToken];
    
    return [NSDictionary dictionaryWithDictionary:headers];
}

@end
