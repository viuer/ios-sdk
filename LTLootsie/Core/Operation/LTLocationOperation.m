//  LTLocationOperation.m
//  Created by Fabio Teles on 7/14/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTLocationOperation.h"
#import "LTLocationCondition.h"
#import "LTSilentCondition.h"
#import "LTMutuallyExclusiveCondition.h"
#import <CoreLocation/CoreLocation.h>

@interface LTLocationOperation () <CLLocationManagerDelegate>

@property (nonatomic, assign) CLLocationAccuracy accuracy;
@property (nonatomic, strong) CLLocationManager *manager;
@property (nonatomic, strong) CLLocation *lastLocation;
@property (nonatomic, strong) void (^locationHandler)(CLLocation *location);

@end

@implementation LTLocationOperation

- (instancetype)initWithAccuracy:(CLLocationAccuracy)accuracy locationHandler:(void (^)(CLLocation *))locationHandler {
    return [self initWithAccuracy:accuracy requestIfNotAvail:YES locationHandler:locationHandler];
}

- (instancetype)initWithAccuracy:(CLLocationAccuracy)accuracy requestIfNotAvail:(BOOL)requestIfNotAvail locationHandler:(void (^)(CLLocation *))locationHandler {
    NSParameterAssert(locationHandler);
    
    if (self = [super init]) {
        _accuracy = accuracy;
        _locationHandler = locationHandler;
        
        if (requestIfNotAvail) {
            [self addCondition:[[LTLocationCondition alloc] initWithUsage:LTLocationUsageWhenInUse]];
        } else {
            [self addCondition:[[LTSilentCondition alloc] initWithCondition:[[LTLocationCondition alloc] initWithUsage:LTLocationUsageWhenInUse]]];
        }
        [self addCondition:[LTMutuallyExclusiveCondition mutualExclusiveForClass:[CLLocationManager class]]];
    }
    return self;
}

- (void)execute {
    dispatch_async(dispatch_get_main_queue(), ^{
        CLLocationManager *manager = [CLLocationManager new];
        manager.desiredAccuracy = self.accuracy;
        manager.delegate = self;
        [manager startUpdatingLocation];
        
        self.manager = manager;
    });
}

- (void)finished:(NSArray *)errors {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self stopLocationUpdates];
        self.locationHandler(self.lastLocation);
    });
}

- (void)stopLocationUpdates {
    [self.manager stopUpdatingLocation];
    self.manager = nil;
}

#pragma mark - CLLocationManagerDelegate
- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    self.lastLocation = locations.lastObject;
    if (self.lastLocation.horizontalAccuracy <= self.accuracy) {
        [self stopLocationUpdates];
        [self finish:nil];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    [self stopLocationUpdates];
    [self finishWithError:error];
}

@end
