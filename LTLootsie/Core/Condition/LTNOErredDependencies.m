//  LTNOErredDependencies.m
//  Created by Fabio Teles on 9/10/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTNOErredDependencies.h"
#import "LTOperation.h"

NSString * const LTErredOperationsKey = @"operationsErred";

@implementation LTNOErredDependencies

- (NSString *)name {
    return NSStringFromClass([self class]);
}

- (BOOL)isMutuallyExclusive {
    return NO;
}

- (NSOperation *)dependencyForOperation:(LTOperation *)operation {
    return nil;
}

- (void)evaluateForOperation:(LTOperation *)operation completion:(void (^)(LTOperationConditionResult, NSError *))completion {
    NSMutableArray *erred = [NSMutableArray array];
    for (LTOperation *dependency in operation.dependencies) {
        if (dependency.generatedErrors.count > 0) {
            [erred addObject:dependency];
        }
    }
    
    if (erred.count > 0) {
        NSError *error = [NSError conditionErrorWithUserInfo:@{NSLocalizedDescriptionKey: NSLocalizedStringFromTable(@"Could not be completed because one or more dependencies erred", @"LTLootsie", nil),
                                                               LTOperationConditionKey: self.name,
                                                               LTErredOperationsKey: erred}];
        completion(LTOperationConditionResultFailed, error);
    } else {
        completion(LTOperationConditionResultSatisfied, nil);
    }
}

@end
