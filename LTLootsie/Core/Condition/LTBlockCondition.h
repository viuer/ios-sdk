//  LTBlockCondition.h
//  Created by Fabio Teles on 9/11/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTOperation.h"

@interface LTBlockCondition : NSObject <LTOperationCondition>

/**
 A simple way to create a condition using a block. You receive on the block to evaluate
 the operation that it's being evaluated against and a pointer to a NSDictionary that
 you can use to provide more information on the error object that will be used to cancel
 the operation dependent on this condition evaluation.
 */
- (instancetype)initWithConditionBlock:(BOOL (^)(LTOperation *operation, NSDictionary * __autoreleasing * errorUserInfo))conditionBlock;

@end
