//  LTData.m
//  Created by Fabio Teles on 7/8/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTData.h"
#import "LTApp.h"
#import "LTConstants.h"

@implementation LTData

@dynamic achievements;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        _version = [[aDecoder decodeObjectOfClass:[NSNumber class] forKey:LTDataStorageVersionKey] integerValue];
        
        _appSecret = [aDecoder decodeObjectOfClass:[NSString class] forKey:LTAppSecretKey];
        _apiSessionToken = [aDecoder decodeObjectOfClass:[NSString class] forKey:LTAPISessionTokenKey];
        _userSessionToken = [aDecoder decodeObjectOfClass:[NSString class] forKey:LTUserSessionTokenKey];
        
        // decoding compatibility, check for older version archive
        if (_version == 0) {
            int startInterval = [aDecoder decodeIntForKey:LTLastSessionStartDateKey];
            int endInterval = [aDecoder decodeIntForKey:LTLastSessionEndDateKey];
            
            _lastSessionStartDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)startInterval];
            _lastSessionEndDate = [NSDate dateWithTimeIntervalSince1970:(NSTimeInterval)endInterval];
        } else {
            _lastSessionStartDate = [aDecoder decodeObjectOfClass:[NSDate class] forKey:LTLastSessionStartDateKey];
            _lastSessionEndDate = [aDecoder decodeObjectOfClass:[NSDate class] forKey:LTLastSessionEndDateKey];
        }
        
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[NSNumber numberWithInteger:LTDataStorageVersion] forKey:LTDataStorageVersionKey];
    
    [aCoder encodeObject:self.appSecret forKey:LTAppSecretKey];
    [aCoder encodeObject:self.apiSessionToken forKey:LTAPISessionTokenKey];
    [aCoder encodeObject:self.userSessionToken forKey:LTUserSessionTokenKey];
    
    [aCoder encodeObject:self.lastSessionStartDate forKey:LTLastSessionStartDateKey];
    [aCoder encodeObject:self.lastSessionEndDate forKey:LTLastSessionEndDateKey];
}

+ (BOOL)supportsSecureCoding
{
    return YES;
}

- (NSArray *)achievements {
    return self.app.achievements;
}

@end