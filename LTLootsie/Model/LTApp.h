//  LTApp.h
//  Created by Fabio Teles on 7/9/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <UIKit/UIKit.h>
#import "LTModelEntity.h"

@class LTImagePath, LTColor;

@interface LTApp : LTModelEntity

@property (nonatomic, strong) NSString *appId;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, assign) NSInteger achievablePoints;
@property (nonatomic, copy) NSArray *achievements;
@property (nonatomic, strong) LTColor *backgroundColor;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, strong) NSArray *engagements;
@property (nonatomic, strong) NSArray *genres;
@property (nonatomic, strong) NSString *hookType;
@property (nonatomic, strong) LTImagePath *imagePaths;
@property (nonatomic, assign) NSInteger moreApps;
@property (nonatomic, strong) NSString *notification;
@property (nonatomic, strong) NSString *orientation;
@property (nonatomic, strong) NSString *posX;
@property (nonatomic, strong) NSString *posY;
@property (nonatomic, strong) LTColor *textColor;
@property (nonatomic, assign) NSInteger totalPointsEarned;

@end
