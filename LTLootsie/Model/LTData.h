//  LTData.h
//  Created by Fabio Teles on 7/8/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTNetworkManager.h"

@class LTApp, LTUserAccount, LTSettings, CLLocation;

@interface LTData : NSObject <NSSecureCoding, LTAuthDataSource>

@property (nonatomic, assign) NSInteger version;

@property (nonatomic, copy) NSString *appSecret;
@property (nonatomic, copy) NSString *apiSessionToken;
@property (nonatomic, copy) NSString *userSessionToken;

@property (nonatomic, strong) NSDate *lastSessionStartDate;
@property (nonatomic, strong) NSDate *lastSessionEndDate;

@property (nonatomic, strong) LTApp *app;
@property (nonatomic, strong) LTSettings *settings;
@property (nonatomic, copy) LTUserAccount *user;

@property (nonatomic, copy) NSArray *rewards;
@property (nonatomic, readonly) NSArray *achievements;

@end
