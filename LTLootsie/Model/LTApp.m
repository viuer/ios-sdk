//  LTApp.m
//  Created by Fabio Teles on 7/9/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"
#import "LTApp.h"
#import "LTColor.h"
#import "LTGenre.h"
#import "LTImagePath.h"
#import "LTEngagement.h"
#if ENABLE_ACHIEVEMENT
#import "LTAchievement.h"
#endif

@implementation LTApp

- (void)populateWithDictionary:(NSDictionary *)dictionary {
    self.appId              = [dictionary objectForKeyNilSafe:LTIdKey];
    self.name               = [dictionary objectForKeyNilSafe:LTNameKey];
    self.achievablePoints   = [[dictionary objectForKeyNilSafe:LTAchievablePointsKey] integerValue];
#if ENABLE_ACHIEVEMENT
    self.achievements       = [LTAchievement collectionWithArray:[dictionary objectForKeyNilSafe:LTAchievementsKey]];
#endif
    self.backgroundColor    = [LTColor entityWithDictionary:[dictionary objectForKeyNilSafe:LTBackgroundColorKey]];
    self.duration           = [[dictionary objectForKeyNilSafe:LTDurationKey] doubleValue];
    self.engagements        = [LTEngagement collectionWithArray:[dictionary objectForKeyNilSafe:LTEngagementsKey]];
    self.genres             = [LTGenre collectionWithArray:[dictionary objectForKeyNilSafe:LTGenderKey]];
    self.hookType           = [dictionary objectForKeyNilSafe:LTHookTypeKey];
    self.imagePaths         = [LTImagePath entityWithDictionary:[dictionary objectForKeyNilSafe:LTImageUrlsKey]];
    self.moreApps           = [[dictionary objectForKeyNilSafe:LTMoreAppsKey] integerValue];
    self.notification       = [dictionary objectForKeyNilSafe:LTNotificationKey];
    self.orientation        = [dictionary objectForKeyNilSafe:LTOrientationKey];
    self.posX               = [dictionary objectForKeyNilSafe:LTPosXKey];
    self.posY               = [dictionary objectForKeyNilSafe:LTPosYKey];
    self.textColor          = [LTColor entityWithDictionary:[dictionary objectForKeyNilSafe:LTTextColorKey]];
    self.totalPointsEarned  = [[dictionary objectForKeyNilSafe:LTTotalPointsEarnedKey] integerValue];
}

@end
