//  LTConstants.m
//  Created by Fabio Teles on 7/7/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import "LTConstants.h"

@implementation LTConstants

///////////////////////////////////////////
// SERVER PATHS AND KEYS
///////////////////////////////////////////

NSString * const LTBaseStagingAPIPath                       = @"https://stage-prime-api.lootsie.com/v2";
NSString * const LTBaseLiveAPIPath                          = @"https://api-v2.lootsie.com/v2";


NSString * const LTWebsitePath                              = @"https://www.lootsie.com/";
NSString * const LTTermsURLComponent                        = @"terms-service";

NSString * const LTHeaderAppSecretKey                       = @"X-Lootsie-App-Secret";
NSString * const LTHeaderAPISessionTokenKey                 = @"X-Lootsie-API-Session-Token";
NSString * const LTHeaderUserSessionTokenKey                = @"X-Lootsie-User-Session-Token";

///////////////////////////////////////////
// GLOBAL STUFF
///////////////////////////////////////////

NSString * const LTPlatform                                 = @"iOS";
NSString * const LTAPIVersion                               = @"v2";
NSString * const LTSDKVersion                               = @"5.1.6.201604111546";
NSString * const LTLocalizationTable                        = @"LTLootsie";

///////////////////////////////////////////
// KEY VALUE CODING
///////////////////////////////////////////

NSInteger const LTDataStorageVersion                        = 1;
NSString * const LTDataStorageVersionKey                    = @"version";

NSString * const LTDatabaseStorageFolder                    = @"com.lootsie";
NSString * const LTDatabaseStorageFilename                  = @"database.lootsie";
NSString * const LTDatabaseStorageOldFolder                 = @"Private Documents";
NSString * const LTDatabaseStorageOldFilename               = @"data.plist";
NSString * const LTDatabaseStorageOldClassName              = @"LootsieData";
NSString * const LTDatabaseStoregeOldDataKey                = @"Data";

NSString * const LTAppSecretKey                             = @"app_secret";
NSString * const LTAPISessionTokenKey                       = @"api_session_token";
NSString * const LTUserSessionTokenKey                      = @"user_session_token";
NSString * const LTLastSessionStartDateKey                  = @"SessionBeginTimestamp";
NSString * const LTLastSessionEndDateKey                    = @"SessionEndTimestamp";

///////////////////////////////////////////
// USER ACTIVITY
///////////////////////////////////////////

NSString * const LTUserActivityMetricSessionStartTime       = @"SessionBeginTime";
NSString * const LTUserActivityMetricSessionEndTime         = @"SessionEndTime";
NSString * const LTUserActivityMetricAchievementIANTapped   = @"AchievementReachedClicked";

///////////////////////////////////////////
// COLORS
///////////////////////////////////////////

NSUInteger const LTMainColor                                = 0xf75352;
NSUInteger const LTDisabledMainColor                        = 0xf7a2a2;

///////////////////////////////////////////
// JSON API KEYS
///////////////////////////////////////////

NSString * const LTAPIDateFormat                            = @"yyyy-MM-dd'T'HH:mm:ss";
NSString * const LTAPIDateOnlyFormat                        = @"yyyy-MM-dd";

NSString * const LTIdKey									= @"id";
NSString * const LTNameKey									= @"name";
NSString * const LTDescriptionKey							= @"description";
NSString * const LTPointsKey                                = @"lp";
NSString * const LTTotalPointsKey                           = @"total_lp";
NSString * const LTIsAchievedKey							= @"is_achieved";
NSString * const LTRepeatableKey							= @"repeatable";
NSString * const LTRewardsKey                               = @"rewards";
NSString * const LTVideoOfferIDKey                          = @"offer_id";
NSString * const LTVideoAchivmentIDKey                      = @"achievement_id";

NSString * const LTAPIVersionKey 							= @"sdk_version";
NSString * const LTLatitudeLongitudeKey                     = @"latlng";
NSString * const LTPlatformKey 								= @"platform";
NSString * const LTDeviceKey 								= @"device";
NSString * const LTFirmwareKey 								= @"firmware";
NSString * const LTLanguageKey 								= @"language";
NSString * const LTCountryKey 								= @"country";

NSString * const LTImageSizeDetailKey						= @"DETAIL";
NSString * const LTImageSizeSKey							= @"S";
NSString * const LTImageSizeMKey							= @"M";
NSString * const LTImageSizeLKey							= @"L";
NSString * const LTImageSizeXLKey							= @"XL";

NSString * const LTBrandNameKey								= @"brand_name";
NSString * const LTEngagementsKey							= @"engagements";
NSString * const LTImageUrlsKey								= @"image_urls";
NSString * const LTIsLimitedTimeKey							= @"is_limited_time";
NSString * const LTIsNewKey									= @"is_new";
NSString * const LTRedemptionsRemainingKey					= @"redemptions_remaining";
NSString * const LTTextToShareKey							= @"text_to_share";
NSString * const LTTosTextKey								= @"tos_text";
NSString * const LTTosUrlKey								= @"tos_url";

NSString * const LTEmailKey									= @"email";
NSString * const LTFirstNameKey								= @"first_name";
NSString * const LTLastNameKey								= @"last_name";
NSString * const LTGenderKey								= @"gender";
NSString * const LTBirthdateKey								= @"birthdate";
NSString * const LTAddressKey								= @"address";
NSString * const LTCityKey									= @"city";
NSString * const LTStateKey									= @"state";
NSString * const LTZipcodeKey								= @"zipcode";
NSString * const LTPhotoURLKey								= @"photo_url";
NSString * const LTConfirmedAgeKey							= @"confirmed_age";
NSString * const LTHomeZipcodeKey							= @"home_zipcode";
NSString * const LTInterestGroupsKey						= @"interest_groups";
NSString * const LTAcceptedTOSKey							= @"accepted_tos";
NSString * const LTLootsieOptinKey							= @"lootsie_optin";
NSString * const LTPartnerOptinKey							= @"partner_optin";
NSString * const LTIsGuestKey								= @"is_guest";

NSString * const LTAchievedPointsKey                        = @"achieved_lp";
NSString * const LTAchievablePointsKey						= @"achievable_lp";
NSString * const LTTotalPointsEarnedKey						= @"total_lp_earned";
NSString * const LTAchievementsKey							= @"achievements";
NSString * const LTDurationKey								= @"duration";
NSString * const LTGenresKey								= @"genres";
NSString * const LTHookTypeKey								= @"hook_type";
NSString * const LTMoreAppsKey								= @"more_apps";
NSString * const LTNotificationKey							= @"notification";
NSString * const LTTextColorKey								= @"text_color";
NSString * const LTBackgroundColorKey						= @"background_color";
NSString * const LTPosXKey									= @"pos_x";
NSString * const LTPosYKey									= @"pos_y";
NSString * const LTOrientationKey							= @"orientation";

NSString * const LTColorRKey                                = @"R";
NSString * const LTColorGKey                                = @"G";
NSString * const LTColorBKey                                = @"B";

NSString * const LTEtagKey                                  = @"etag";
NSString * const LTSettingsKey                              = @"settings";
NSString * const LTCurrencyNameKey							= @"currency_name";
NSString * const LTSDKEnabledKey							= @"sdk.enabled";
NSString * const LTCurrencyAbbreviationKey					= @"currency_abbreviation";
NSString * const LTRewardsPageSizeKey						= @"sdk.api.rewards.page-size";

NSString * const LTMetricKey								= @"metric";
NSString * const LTValueKey									= @"value";
NSString * const LTTimeKey									= @"time";

NSString * const LTRewardIdsKey                             = @"reward_ids";
NSString * const LTAchievementIdsKey                        = @"achievement_ids";
NSString * const LTRewardRedemptionIdKey                    = @"reward_redemption_id";

NSString * const LTErrorErrorsKey                           = @"errors";
NSString * const LTErrorFieldKey                            = @"field";
NSString * const LTErrorMessageKey                          = @"message";

@end
