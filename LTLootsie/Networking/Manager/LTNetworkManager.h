//  LTNetworkManager.h
//  Created by Fabio Teles on 7/16/15.
//  Copyright (c) 2015 Lootsie Inc. Copyright All Rights Reserved ( http://www.lootsie.com )

//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:

//  The above copyright notice and this permission notice shall be included in
//  all copies or substantial portions of the Software.

//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.

#import <Foundation/Foundation.h>

@class LTOperation, LTOperationQueue;

extern NSString * const LTAPISessionPath;
extern NSString * const LTAPISettingsPath;
extern NSString * const LTAPIAppPath;

extern NSString * const LTAPIUserSessionPath;
extern NSString * const LTAPIUserSessionGuestPath;
extern NSString * const LTAPIUserSessionEmailOnlyPath;
extern NSString * const LTAPIUserAccountPath;
extern NSString * const LTAPIUserAchievementsPath;
extern NSString * const LTAPIUserRewardPath;
extern NSString * const LTAPIUserRewardRedemptionsPath;
extern NSString * const LTAPIUserRewardEmailRedemptionPath;
extern NSString * const LTAPIUserLocationPath;
extern NSString * const LTAPIUserActivityPath;
extern NSString * const LTAPIVideoInterstitialPath;
extern NSString * const LTAPIVideoInterstitialAchivmentIANPath;

extern NSString * const LTNetworkErrorDomain;
extern NSString * const LTNetworkResponseErrorsJSONKey;

typedef NS_ENUM(NSUInteger, LTTypeServer) {
    LTLifeTypeServer,
    LTStageTypeServer
};

@protocol LTAuthDataSource <NSObject>

- (NSString *)appSecret;
- (NSString *)apiSessionToken;
- (NSString *)userSessionToken;

@end

@class LTAFNetworkReachabilityManager;
@interface LTNetworkManager : NSObject

@property (nonatomic, readonly) NSURL *baseURL;
@property (nonatomic, readonly) LTAFNetworkReachabilityManager *reachabilityManager;
@property (nonatomic, strong) id <LTAuthDataSource> authDataSource;
@property (nonatomic, readonly, strong) LTOperationQueue *networkQueue;

+ (instancetype)lootsieManager;
+ (LTTypeServer)typeServer;

- (void)addNetworkOperation:(LTOperation *)operation;
- (LTOperation *)lastQueuedNonFinishedOperationOfType:(Class)operationType;

- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                   path:(NSString *)path
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error;

- (NSURLRequest *)requestWithHTTPMethod:(NSString *)method
                                    url:(NSString *)urlString
                             parameters:(id)parameters
                  additionalHTTPHeaders:(NSDictionary *)additionalHTTPHeaders
                                  error:(NSError *__autoreleasing *)error;

- (NSURLSessionDataTask *)dataTaskWithRequest:(NSURLRequest *)request
                            completionHandler:(void (^)(NSURLResponse *response, id responseObject, NSError *error))completionHandler;

@end

